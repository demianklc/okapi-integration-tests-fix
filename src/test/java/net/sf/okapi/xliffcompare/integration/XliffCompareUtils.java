package net.sf.okapi.xliffcompare.integration;

import java.io.File;

public final class XliffCompareUtils {
	public static final String CURRENT_XLIFF_ROOT = new File(XliffCompareUtils.class.getResource("/XLIFF_PREV/dummy.txt").getPath()).getParent() + File.separator;
}
