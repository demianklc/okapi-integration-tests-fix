package net.sf.okapi.simplifier.integration;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;

import net.sf.okapi.common.LocaleId;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.ZipXMLFileCompare;
import net.sf.okapi.common.integration.IntegrationtestUtils;
import net.sf.okapi.common.integration.RoundTripUtils;
import net.sf.okapi.filters.openxml.OpenXMLFilter;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RoundTripSimplifyOpenXmlTkitsIT
{
	private final Logger LOGGER = LoggerFactory.getLogger(getClass());

	private OpenXMLFilter openXmlFilter;
	
	@Rule
	public ErrorCollector errCol = new ErrorCollector();

	@Before
	public void setUp() throws Exception {
		openXmlFilter = new OpenXMLFilter();
	}

	@After
	public void tearDown() throws Exception {
		openXmlFilter.close();
	}
	
	@Ignore("placeholder to test single file")
	public void singleFile() throws FileNotFoundException, URISyntaxException {		
		for (File file : IntegrationtestUtils.getTestFiles("/openxml/", Arrays.asList(".docx_FAILURE"))) {
			runTest(true, file, "okf_openxml", null);
		}
	}

	@Test
	public void openXmlFiles() throws FileNotFoundException, URISyntaxException {		
		// run top level files (without config)
		for (File file : IntegrationtestUtils.getTestFilesNoRecurse("/openxml/", Arrays.asList(".docx", ".pptx", ".xlsx"))) {
			runTest(true, file, "okf_openxml", null);
		}

		// run each subdirectory where we assume there is a custom config)
		for(File d : IntegrationtestUtils.getSubDirs("/openxml/"))
		{
			for(File c : IntegrationtestUtils.getConfigFile(d.getPath()))
			{
				for(File file : IntegrationtestUtils.getTestFiles(d.getPath(), Arrays.asList(".docx", ".pptx", ".xlsx"), true))
				{					
					String configName = Util.getFilename(c.getAbsolutePath(), false);
					String customConfigPath = c.getParent();
					runTest(true, file, configName, customConfigPath);
				}
			}
		}
	}

	private void runTest(boolean segment, File file, String configName, String customConfigPath)
			throws FileNotFoundException, URISyntaxException {
		String f = file.getName();
		LOGGER.info(f);
		String root = file.getParent() + File.separator;
		String xliff = root + f + ".simplify_xliff";
		String original = root + f;
		String tkitMerged = root + f + ".tkitMerged";
		String merged = root + f + ".merged";
		
		RoundTripUtils.extract(LocaleId.ENGLISH, LocaleId.FRENCH, original, xliff, configName, customConfigPath, segment, false);		
		RoundTripUtils.merge(LocaleId.ENGLISH, LocaleId.FRENCH, false, original, xliff, merged, configName, customConfigPath);
		
		RoundTripUtils.extract(LocaleId.ENGLISH, LocaleId.FRENCH, original, xliff, configName, customConfigPath, segment, true);		
		RoundTripUtils.merge(LocaleId.ENGLISH, LocaleId.FRENCH, false, original, xliff, tkitMerged, configName, customConfigPath);
		
		ZipXMLFileCompare compare = new ZipXMLFileCompare();		
		try {
			assertTrue("Compare Lines: " + f, compare.compareFiles(merged, tkitMerged));
		} catch(Throwable e) {
			errCol.addError(e);
			LOGGER.info("FAILURE:" + f);
		}
	}
}
